package com.epam.trainings.main;

import com.epam.trainings.mvc.controller.TestController;
import com.epam.trainings.mvc.controller.ViewController;
import com.epam.trainings.mvc.model.Car;
import com.epam.trainings.mvc.model.Email;
import com.epam.trainings.mvc.model.NotEntity;
import com.epam.trainings.mvc.model.User;
import com.epam.trainings.mvc.model.menu.Menu;
import com.epam.trainings.mvc.model.menu.SimpleMainMenu;
import com.epam.trainings.mvc.view.View;

import java.util.ArrayList;

public class Main {
  public static void main(String[] args) {
    User user = new User();
    NotEntity notEntity = new NotEntity();
    Email email = new Email();
    Car car = new Car();
    ArrayList<Object> tableEntities = new ArrayList<>();
    tableEntities.add(user);
    tableEntities.add(email);
    tableEntities.add(car);
    tableEntities.add(notEntity);
    View view = new View();
    SimpleMainMenu menu = new Menu();
    ViewController viewController = new ViewController(tableEntities, view, menu);
    TestController testController = new TestController();
    view.setController(viewController);
  }
}
