package com.epam.trainings.mvc.view;

import com.epam.trainings.annotations.annotationhandlers.ControllerHandler;
import com.epam.trainings.command.ChangeLanguageCommand;
import com.epam.trainings.mvc.controller.SimpleController;
import com.epam.trainings.exceptions.ControllerNotFoundException;
import com.epam.trainings.utils.TableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class View {
  private static Logger log = LogManager.getLogger(View.class.getName());
  private SimpleController controller;
  private boolean firstStart;
  private Scanner scanner;

  public void setController(SimpleController controller) {
    firstStart = true;
    scanner = new Scanner(System.in);
    if (ControllerHandler.isRightController(controller, this)) {
      this.controller = controller;
      show();
    } else {
      throw new IllegalArgumentException("Invalid controller!");
    }
  }

  public void modelChanged() {
    if (firstStart) firstStart = !firstStart;
    show();
  }

  public void show() {
    if (controller == null) {
      try {
        throw new ControllerNotFoundException("set controller for view");
      } catch (ControllerNotFoundException e) {
        e.printStackTrace();
      }
    }
    if (firstStart) {
      controller.execute(1);
    }
    String toShow = controller.showMenu();
    System.out.println(toShow);
    askForCommand();
  }

  public void show(String stringToShow) {
    log.info(stringToShow);
  }

  private void askForCommand() {
    log.info("Ask for command");
    int command;
    command = scanner.nextInt();
    controller.execute(command);
  }

  public void showTable(String name, ArrayList<String> column, List<String> listOfFields) {
    TableList tableList = new TableList(column);
    String[] fields = listOfFields.toArray(new String[listOfFields.size()]);
    tableList.addRow(fields);
    log.info("\nTable name : " + name);
    tableList.print();
    System.out.println("\n\n");
  }
}
