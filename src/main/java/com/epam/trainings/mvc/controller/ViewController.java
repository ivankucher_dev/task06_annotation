package com.epam.trainings.mvc.controller;

import com.epam.trainings.annotations.annotationhandlers.ColumnHandler;
import com.epam.trainings.annotations.annotationhandlers.IdHandler;
import com.epam.trainings.annotations.annotationhandlers.InitDefaultHandler;
import com.epam.trainings.annotations.classannotations.Controller;
import com.epam.trainings.annotations.tableannotations.Entity;
import com.epam.trainings.command.ChangeLanguageCommand;
import com.epam.trainings.command.CommandExecutor;
import com.epam.trainings.exceptions.TableCreationException;
import com.epam.trainings.mvc.model.menu.Language;
import com.epam.trainings.mvc.model.menu.SimpleMainMenu;
import com.epam.trainings.mvc.model.table.Table;
import com.epam.trainings.mvc.view.View;
import static com.epam.trainings.utils.PropertiesGetter.*;

import org.apache.logging.log4j.core.tools.picocli.CommandLine;
import org.reflections.Reflections;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Stream;

@Controller(view = View.class)
public class ViewController implements SimpleController {

  private SimpleMainMenu menu;
  private View view;
  private Object entity;
  private List<Object> tableEntities;
  private Properties properties;
  private List<Table> tables;
  private Language language;
  private CommandExecutor executor;

  public ViewController(List<Object> tableEntities, View view, SimpleMainMenu menu) {
    this.menu = menu;
    this.language = new Language(this.menu);
    this.view = view;
    this.tableEntities = tableEntities;
    tables = new ArrayList<>();
    properties = getProperties();
    executor = new CommandExecutor();
    getTables();
  }

  public ViewController(Object entity, View view, SimpleMainMenu menu) {
    this.menu = menu;
    this.view = view;
    this.entity = entity;
    properties = getProperties();
    executor = new CommandExecutor();
    getTable(entity);
  }

  public List<Table> getTables() {
    tableEntities.forEach(entity -> initTable(entity));
    return tables;
  }

  public Table getTable(Object object) {
    if (entity == null) {
      throw new NullPointerException("U must use another constructor");
    }
    initTable(object);
    return tables.get(0);
  }

  public String showMenu() {
    return menu.show();
  }

  public String showTable() {
    StringBuilder sb = new StringBuilder();
    if (tables.isEmpty()) {
      return "no tables found";
    }
    tables.forEach(
        table -> {
          sb.append("\nTable name : " + table.getName() + "\n");
          table.getTableColumns().forEach(column -> sb.append("| " + column + " | "));
          sb.append("\n");
          if (!table.getDefaultValues().isEmpty()) {
            table
                .getDefaultValues()
                .forEach(
                    value -> {
                      sb.append("   " + value);
                    });
            view.showTable(table.getName(), table.getTableColumns(), table.getDefaultValues());
          }
          sb.append("\n");
        });
    return sb.toString();
  }

  public String execute(int command) {
    String result = "";
    final int change_language = Integer.valueOf(properties.getProperty("change_language"));
    final int show_table = Integer.valueOf(properties.getProperty("show_table"));
    if (command == change_language) {
      result = executor.execute(new ChangeLanguageCommand(language));
    }
    if (command == show_table) {
      result = showTable();
    }
    view.show(result);
    view.modelChanged();
    return result;
  }

  private void initTable(Object object) {
    if (Objects.isNull(object)) {
      throw new TableCreationException("The object to create table is null");
    }

    Class<?> clazz = object.getClass();
    if (clazz.isAnnotationPresent(Entity.class)) {
      tables.add(fillTableByObject(clazz, object));
    }
  }

  private Table fillTableByObject(Class<?> clazz, Object object) {
    String tableName = clazz.getSimpleName();
    Table table = new Table(tableName);
    Reflections reflections = new Reflections(properties.getProperty("tableAnnotationsPath"));
    Set<Class<? extends Annotation>> allClasses = reflections.getSubTypesOf(Annotation.class);
    allClasses.remove(Entity.class);
    checkFieldsByAnnotations(clazz.getDeclaredFields(), allClasses, table);
    checkMethods(clazz.getDeclaredMethods(), allClasses, table, object);
    return table;
  }

  private void checkFieldsByAnnotations(
      Field[] fields, Set<Class<? extends Annotation>> allClasses, Table table) {
    boolean column_added;
    for (Field field : fields) {
      column_added = false;
      for (Class annotation : allClasses) {
        if (field.isAnnotationPresent(annotation)) {
          table.addColumn(redirectForSpecificMethod(annotation, field));
          column_added = true;
        }
      }
      if (!column_added) {
        table.addColumn(field.getName());
      }
    }
  }

  private void checkMethods(
      Method[] methods, Set<Class<? extends Annotation>> allClasses, Table table, Object object) {
    Stream.of(methods)
        .forEach(
            method -> {
              allClasses.forEach(
                  annotation -> {
                    if (method.isAnnotationPresent(annotation)) {
                      redirectForSpecificMethod(annotation, method, table, object);
                    }
                  });
            });
  }

  private String redirectForSpecificMethod(Class annotation, Field field) {
    String columnName = null;
    if (annotation.getSimpleName().equals(properties.getProperty("annotation_Id"))) {
      columnName = IdHandler.handleIdField(field);
    } else if (annotation.getSimpleName().equals(properties.getProperty("annotation_Column"))) {
      columnName = ColumnHandler.handleColumnField(field);
    }
    if (columnName == null) {
      throw new NullPointerException("Your ENTITY class should be specific annotated");
    }
    return columnName;
  }

  private void redirectForSpecificMethod(
      Class annotation, Method method, Table table, Object object) {
    if (annotation.getSimpleName().equals(properties.getProperty("annotation_InitDefaults"))) {
      table.setDefaultValues(InitDefaultHandler.handleInitDefault(method, object));
    }
  }
}
