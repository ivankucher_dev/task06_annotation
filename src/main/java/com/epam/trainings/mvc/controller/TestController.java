package com.epam.trainings.mvc.controller;

import com.epam.trainings.annotations.classannotations.Controller;
import com.epam.trainings.mvc.model.menu.Menu;
import com.epam.trainings.mvc.model.table.Table;

import java.util.List;

@Controller(view = Menu.class)
public class TestController implements SimpleController {

  @Override
  public List<Table> getTables() {
    return null;
  }

  @Override
  public Table getTable(Object object) {
    return null;
  }

  @Override
  public String showMenu() {
    return null;
  }

  @Override
  public String execute(int command) {
    return null;
  }

  @Override
  public String showTable() {
    return null;
  }
}
