package com.epam.trainings.mvc.model.table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Table {
  private String name;
  private ArrayList<String> tableColumns;
  private List<String> defaultValues;

  public Table(String name) {
    this.name = name;
    tableColumns = new ArrayList<>();
    defaultValues = new ArrayList<>();
  }

  public ArrayList<String> getTableColumns() {
    return tableColumns;
  }

  public void addColumn(String column) {
    this.tableColumns.add(column);
  }

  public void setDefaultValues(List<String> defaultValues) {
    this.defaultValues = defaultValues;
  }

  public List<String> getDefaultValues() {
    return defaultValues;
  }

  public String getName() {
    return name;
  }
}
