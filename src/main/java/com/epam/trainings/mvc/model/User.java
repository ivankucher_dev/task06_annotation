package com.epam.trainings.mvc.model;

import com.epam.trainings.annotations.tableannotations.Column;
import com.epam.trainings.annotations.tableannotations.Entity;
import com.epam.trainings.annotations.tableannotations.Id;
import com.epam.trainings.annotations.tableannotations.InitDefault;

import static com.epam.trainings.utils.PropertiesGetter.getIntProp;
import static com.epam.trainings.utils.PropertiesGetter.getProperties;

@Entity
public class User {

  @Id private int user_id;

  @Column(name = "user_name")
  private String name;

  @Column(name = "user_age")
  private int age;

  @Column private String surname;

  @InitDefault
  private void initDefaultValues() {
    user_id = getIntProp("default_id");
    name = getProperties().getProperty("default_string");
    age = getIntProp("default_int");
    surname = getProperties().getProperty("default_string");
  }

  public int getId() {
    return user_id;
  }

  public void setId(int id) {
    this.user_id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }
}
