package com.epam.trainings.mvc.model;

import com.epam.trainings.annotations.tableannotations.Column;
import com.epam.trainings.annotations.tableannotations.Entity;
import com.epam.trainings.annotations.tableannotations.Id;
import com.epam.trainings.annotations.tableannotations.InitDefault;

import static com.epam.trainings.utils.PropertiesGetter.*;

@Entity
public class Car {

  @Id private int id;

  @Column(name = "car_name")
  private String name;

  private long number;

  @InitDefault
  private void initDefaultValues() {
    id = getIntProp("default_id");
    name = getProperties().getProperty("default_string");
    number = getIntProp("default_int");
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getNumber() {
    return number;
  }

  public void setNumber(long number) {
    this.number = number;
  }
}
