package com.epam.trainings.mvc.model;

import com.epam.trainings.annotations.tableannotations.Column;
import com.epam.trainings.annotations.tableannotations.Entity;
import com.epam.trainings.annotations.tableannotations.Id;

@Entity
public class Email {

  @Id private int id;
  private String email;

  @Column(name = "email_password")
  private int password;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public int getPassword() {
    return password;
  }

  public void setPassword(int password) {
    this.password = password;
  }
}
