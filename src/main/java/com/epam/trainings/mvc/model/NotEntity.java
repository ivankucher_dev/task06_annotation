package com.epam.trainings.mvc.model;

public class NotEntity {

  private int id;
  private long smth;
  private String string;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public long getSmth() {
    return smth;
  }

  public void setSmth(long smth) {
    this.smth = smth;
  }

  public String getString() {
    return string;
  }

  public void setString(String string) {
    this.string = string;
  }
}
