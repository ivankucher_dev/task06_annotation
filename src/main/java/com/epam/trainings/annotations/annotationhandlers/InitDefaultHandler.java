package com.epam.trainings.annotations.annotationhandlers;

import com.epam.trainings.mvc.model.table.Table;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InitDefaultHandler {
  public static List<String> handleInitDefault(Method method, Object object) {
    System.out.println(method.getName());
    ArrayList<Object> arrayList = new ArrayList<>();
    method.setAccessible(true);
    try {
      method.invoke(object);
    } catch (Exception e) {
      e.printStackTrace();
    }
    Class<?> clazz = object.getClass();
    Stream.of(clazz.getDeclaredFields())
        .forEach(
            field -> {
              field.setAccessible(true);
              try {
                arrayList.add(field.get(object));
              } catch (IllegalAccessException e) {
                e.printStackTrace();
              }
            });

    return arrayList.stream().map(Object::toString).collect(Collectors.toList());
  }
}
