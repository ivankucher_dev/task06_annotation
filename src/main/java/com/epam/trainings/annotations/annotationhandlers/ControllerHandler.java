package com.epam.trainings.annotations.annotationhandlers;

import com.epam.trainings.annotations.classannotations.Controller;

public class ControllerHandler {
  public static boolean isRightController(Object controller, Object view) {
    Class<?> clazz = controller.getClass();
    if (clazz.isAnnotationPresent(Controller.class)) {
      Controller c = (Controller) clazz.getAnnotation(Controller.class);
      Class controller_view = c.view();
      if (controller_view.equals(getViewClass(view))) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  private static Class getViewClass(Object view) {
    return view.getClass();
  }
}
