package com.epam.trainings.annotations.annotationhandlers;

import com.epam.trainings.annotations.tableannotations.Id;

import java.lang.reflect.Field;

public class IdHandler {
    public static String handleIdField(Field field){
    String id = field.getName();
    return id;
    }
}
