package com.epam.trainings.command;

import com.epam.trainings.mvc.model.menu.Language;

import java.util.Locale;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ChangeLanguageCommand implements Command {
    private Language language;
  private static Logger log = LogManager.getLogger(ChangeLanguageCommand.class.getName());

    public ChangeLanguageCommand(Language language) {
        this.language = language;
    }

    @Override
    public String execute() {
        Scanner scanner = new Scanner(System.in);
        int choose;
        String localeLang;
        log.info("Choose your language : ");
        language.showLanguageList();
        do {
            log.info("input num of language : ");
            choose = scanner.nextInt();
            localeLang = language.getAvaliableLanguages().get(choose);
        } while (localeLang == null);
        language.updateLanguage(new Locale(localeLang));
        return "language changed";
    }
}
