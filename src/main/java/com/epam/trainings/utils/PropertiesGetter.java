package com.epam.trainings.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesGetter {

  public static Properties getProperties() {
    Properties prop = null;
    try (InputStream input =
        PropertiesGetter.class.getClassLoader().getResourceAsStream("config.properties")) {

      prop = new Properties();

      if (input == null) {
        System.out.println("Sorry, unable to find config.properties");
        return null;
      }
      prop.load(input);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return prop;
  }

  public static int getIntProp(String prop) {
    return Integer.valueOf(getProperties().getProperty(prop));
  }
}
